jQuery(document).ready(function() {
	
	// update price when quantity changed
	jQuery('.Cart66UserQuantity').on('input', function(){
		var current_object = jQuery(this).siblings('select[name=options_1]');
		priceUpdate(current_object);
	});
	
	// update price when new option chosen
	jQuery('select[name=options_1]').on('click', function(){	
		var current_object = this;
		priceUpdate(current_object);
	});
	
	function priceUpdate(current_object){
		// get default price
		jQuery.get(mylocalizedscript.myurl, {
			
			'item' : jQuery(current_object).prevAll('input[name=cart66ItemId]').val()
			
		},function(data){
			
			if(data){
				var price = parseFloat(data, 10);
				var select_price = jQuery(current_object).find('option:selected').val()
				
				if(select_price.indexOf('+') != -1){ //check if default price
					var price_arr = select_price.split('+');
					price_arr = price_arr[1].replace('$', '');
					var new_price = parseFloat(price, 10)+parseFloat(price_arr, 10);
				}else{ // if default price
					var new_price = price;
				}
				
				// get quantity
				var quantity = jQuery(current_object).siblings('.Cart66UserQuantity').find('input[name=item_quantity]').val();
				
				// multiply new price by quantity
				new_price = new_price * parseInt(quantity, 10);
				
				console.log(quantity);
				
				// insert new price
				if(new_price != 'NaN'){
					jQuery(current_object).siblings('span.Cart66PriceBlock').find('.Cart66PreDecimal').text(new_price.toFixed(2));
					jQuery(current_object).siblings('span.Cart66PriceBlock').find('.Cart66PostDecimal').text('');
					jQuery(current_object).siblings('span.Cart66PriceBlock').find('.Cart66DecimalSep').text('');
				}
			}
			else{return false;}
		});
	}
});
