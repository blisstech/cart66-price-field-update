<?php
defined('ABSPATH') or die("No script kiddies please!");
/**
 * Plugin Name: Cart66 Price update fix
 * Plugin URI: http://blisstech.co
 * Description: Updates the price when a new option is selected from the drop down
 * Version: The Plugin's Version Number,  1.0
 * Author: Derek Bliss
 * Author URI: http://blisstech.co
 * License: GPL2
 */
 /*  Copyright YEAR  PLUGIN_AUTHOR_NAME  (email : PLUGIN AUTHOR EMAIL)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// inlcudes
wp_enqueue_script('the_js', plugins_url('/js/custom.js', __FILE__),'','',true);
wp_localize_script('the_js', 'mylocalizedscript', array('myurl' => plugins_url('include/price.php', __FILE__)));

wp_enqueue_style('the_css', plugins_url( '/css/custom.css', __FILE__ ));
?>
