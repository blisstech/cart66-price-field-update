# README #

Cart66 price field update
This plugin must be used with the cart66 pro downloadable plugin located at http://cart66.com/
This plugin will automatically update a product price when a new pricing option is selected or the quantity is changed. View demo http://youtu.be/g523puiK4js


In order for this plugin to work you must only put your pricing options in the options 1 group and any other product option in the options 2 group.
 ![tng23.jpg](https://bitbucket.org/repo/Kz9k57/images/1431490371-tng23.jpg)
 
